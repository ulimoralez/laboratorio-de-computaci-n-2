package Desafio8;

import java.util.Scanner;
import java.lang.Math;

public class Desafio8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese un número para sacar su raíz cuadrada :");
        RaizScanner num = new RaizScanner(sc.nextInt());
        System.out.println("La raíz cuadrada de "+num.getN()+" es: "+num.obtenerRaiz());
    }
}
