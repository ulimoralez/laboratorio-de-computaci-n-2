package Desafio8;
public class RaizScanner {
    int n;
    public RaizScanner(){
    }
    public RaizScanner(int n) {
       this.n = n;
    }
    public int getN() {
        return n;
    }
    public void setN(int n) {
        this.n = n;
    }
    public double obtenerRaiz(){
        double res = Math.sqrt(this.n);
        return res;
    }
}
