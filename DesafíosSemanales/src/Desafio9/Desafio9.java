package Desafio9;

import java.util.Scanner;

public class Desafio9 {
    
    public static double calcularPesoIdeal(char gender, int altura){
        double pesoideal = 0;
        if (gender == 'm'){
           pesoideal = altura - 120;
        }else{
            pesoideal = altura - 110;
        }
        return pesoideal;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int altura;
        char gender;
        int pesoideal;
        
        System.out.println("Ingrese su sexo M o H");
        gender = sc.next().charAt(0);
        Character.toLowerCase(gender);
        
        switch (gender){
            case 'm':
                System.out.println("Ingrese su altura: ");
                altura = sc.nextInt();
                System.out.println("Su peso ideal seria: "+calcularPesoIdeal(gender, altura));
                break;
            case 'h':
                System.out.println("Ingrese su altura: ");
                altura = sc.nextInt();
                System.out.println("Su peso ideal seria: "+calcularPesoIdeal(gender, altura));
                break;
            default:
                System.out.println("Error bro, pifiaste en el género");
        }
    
    }
}