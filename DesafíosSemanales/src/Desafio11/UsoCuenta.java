package Desafio11;

/**
 *
 * @author moral
 */
public class UsoCuenta {

    public static void main(String[] args) {
        CuentaCorriente Cuenta1 = new CuentaCorriente("Cuenta1", 10000);
        CuentaCorriente Cuenta2 = new CuentaCorriente("Cuenta2", 0);
        
        Cuenta1.Transferir(Cuenta1, Cuenta2, 2500);
        
        System.out.println(Cuenta1.mostrarDatos());
        System.out.println(Cuenta2.mostrarDatos());
    }
}
