package Desafio11;

import java.lang.Math;
import java.util.Random;

public class CuentaCorriente {

    private double saldo;
    private String nombreTitular;
    private long numeroCuenta;

    public CuentaCorriente(String nombreTitular, double saldo){
        this.nombreTitular = nombreTitular;
        this.saldo = saldo;
        Random rd = new Random();
        long prueba = rd.nextLong();
        if(prueba < 0){
            prueba = prueba * -1;
            numeroCuenta = prueba;
        }else{
            numeroCuenta = prueba;
        }
    }

    //Setters
    public void Ingresos(double ingreso) {
        this.saldo = saldo + ingreso;
    }

    public void Extracción(double monto) {
        this.saldo = saldo - monto;
    }

    //Getters
    public double getSaldo() {
        return saldo;
    }

    public String mostrarDatos() {
        return "Nombre del titular: " + nombreTitular + "\nNúmero de cuenta: " + numeroCuenta + "\nSaldo: $" + saldo;
    }

    public boolean Transferir(CuentaCorriente emisor, CuentaCorriente receptor, double cantidad) {
        boolean correcto = true;
        if (cantidad < 0) {
            correcto = false;
        } else if (saldo >= cantidad) {
            emisor.Extracción(cantidad);
            receptor.Ingresos(cantidad);
        } else {
            correcto = false;
        }
        return correcto;
    }
}
