package Desafio7;
import java.lang.Math;

public class Desafio7 {
    public static void main(String[] args) {
        /*Declaración de variables*/
        /*Funciones trigonométricas habituales*/
        double mathsin = Math.sin(50.50);
        double mathcos = Math.cos(50.50);
        double mathatan = Math.atan(50.50);
        double mathatan2 = Math.atan2(50.50, 30.30);
        /*La función exponencial y su inversa*/
        double mathexp = Math.exp(50.50);
        double mathlog = Math.log(50.50);
        /*Las dos constantes PI y e*/
        double pi = Math.PI;
        double e = Math.E;

        /*Mostrando resultados*/
        System.out.println("Usando Math.sin con el numero 50.50 el resultado es : "+mathsin);
        System.out.println("Usando Math.cos con el numero 50.50 el resultado es: "+mathcos);
        System.out.println("Usando Math.atan con el numero 50.50 el resultado es: "+mathatan);
        System.out.println("Usando Math.atan2 con el numero 50.50 el resultado es: "+mathatan2);
        System.out.println("Usando Math.exp con el numero 50.50 el resultado es: "+mathexp);
        System.out.println("Usando Math.log con el numero 50.50 el resultado es: "+mathlog);
        System.out.println("Usando Math.PI el resultado es: "+pi);
        System.out.println("Usando Math.E el resultado es: "+e);
    }
}
