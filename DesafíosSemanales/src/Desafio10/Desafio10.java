package Desafio10;

import java.lang.Math;
import java.util.Scanner;

public class Desafio10 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numero_adivinar;
        int numing;
        int limite = 100;
        boolean flag = false;

        numero_adivinar = (int) (Math.random() * limite);

        //System.out.println("El número es: " + numero_adivinar);
        while (!flag) {
            System.out.println("Ingrese un número: ");
            numing = sc.nextInt();
            if (numing > 100 || numing < 0) {
                System.out.println("Su número no puede ser menor que 0 ni mayor a 100\n");
                continue;
            }
            if (numing < numero_adivinar) {
                System.out.println("Su número es menor al que tiene que adivinar: ");
            } else {
                if (numing > numero_adivinar) {
                    System.out.println("Su número es mayor al que tiene que adivinar: ");
                }
            }
            if (numing == numero_adivinar) {
                System.out.println("Felicidades, adivinaste el número");
                flag = true;
            }
        }
    }
}
