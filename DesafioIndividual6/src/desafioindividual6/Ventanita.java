package desafioindividual6;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
/**
 *
 * @author Ulises Moralez - ulimoralez
 */


public class Ventanita extends JFrame{
    //Declaramos lo básico de nuestro marco
    public Ventanita(){
        this.setVisible(true);
        this.setSize(600, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
    }
    //Acá cargamos todo los componentes
    public void iniciarComponentes(){
        //Creamos el panel
        panel pan1 = new panel();
        
        ClickEvents focus = new ClickEvents(pan1);
        pan1.addMouseListener(focus);
        this.add(pan1);
        //Aañadimos un título a nuestra ventana
        this.setTitle("Moralez Ulises");
        //Le agregamos un tamaño minimo
        this.setMinimumSize(new Dimension(500,500));
    }
    class ClickEvents implements MouseListener{
        JPanel panel = new JPanel();
        public ClickEvents(JPanel p){
            this.panel = p;
        }

        @Override
        public void mouseClicked(MouseEvent me) {
            panel.setBackground(Color.yellow);
        }

        @Override
        public void mousePressed(MouseEvent me) {
            panel.setBackground(Color.white);
        }

        @Override
        public void mouseReleased(MouseEvent me) {
            panel.setBackground(Color.red);
        }

        @Override
        public void mouseEntered(MouseEvent me) {
            panel.setBackground(Color.blue);
        }

        @Override
        public void mouseExited(MouseEvent me) {
            panel.setBackground(Color.green);
        }

         
     }
}
//Creando nuestro panel
 class panel extends JPanel{
     
     JButton b1 = new JButton("Hello");
     JButton b2 = new JButton("Press me");
     //Creamos UN SOLO listener para que así este diferencia qué objeto la instancia.
     FocusEvents foco = new FocusEvents();
     public panel(){
         add(b1);
         add(b2);
         //Al hacer click - El Boton hace - El this se refiere a la clase donde nos encontramos, a panel
         //Y pasamos como objeto el listener este que creamos en la linea 37
         b1.addFocusListener(foco);
         b2.addFocusListener(foco);
     }
     //Creamos la clase DENTRO DEL PANEL. si, clases dentro de clases.
     class FocusEvents implements FocusListener{
        public FocusEvents(){
            
        }
        @Override
        public void focusGained(FocusEvent fe) {
            if(fe.getSource()==b1){
                System.out.println("Button Hello on focus");
            }else{
                System.out.println("Button Watch me on focus");
            }
        }

        @Override
        public void focusLost(FocusEvent fe) {
            
        }
         
     }
     
     
}
/*class FocusEvents implements FocusListener{
    
    public FocusEvents(Object e){
        
    }

    @Override
    public void focusGained(FocusEvent fe) {
        Object focus = fe.getSource();
        if(focus=="b1"){
            System.out.println("Hello Watched");
        }else{
            System.out.println("Press Me Watched");
        }
    }

    @Override
    public void focusLost(FocusEvent fe) {
        Object unfocus = fe.getSource();
        if(unfocus == "ev1"){
            System.out.println("Hello Don't Watched");
        }else{
            System.out.println("Press Me Don't Watched");
        }
    }
    
}
*/