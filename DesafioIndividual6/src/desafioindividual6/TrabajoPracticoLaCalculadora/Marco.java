package desafioindividual6.TrabajoPracticoLaCalculadora;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Ulises Moralez - ulimoralez
 */
public class Marco extends JFrame {

    public Marco() {

        setVisible(true);
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setTitle("Calculadora");
        setMinimumSize(new Dimension(500, 500));

        Panel panel = new Panel();
        add(panel);

    }

}

class Panel extends JPanel {
    JButton pant = new JButton("0");
    JPanel panelNumeros = new JPanel();
    boolean start = true;
    String operacion;
    double total;

    public Panel() {

        setLayout(new BorderLayout());
        pant.setEnabled(false);
        //Se añade la pantalla a la parte superior
        add(pant, BorderLayout.NORTH);

        panelNumeros.setLayout(new GridLayout(4, 5));
        
        limpiarPantalla clean = new limpiarPantalla();
        addNumeros listener = new addNumeros();
        Operacion op = new Operacion();
        
        newButton("1",listener);
        newButton("2",listener);
        newButton("3",listener);
        newButton("+",op);
        
        
        newButton("4",listener);
        newButton("5",listener);
        newButton("6",listener);
        newButton("-",op);
        
        
        newButton("7",listener);
        newButton("8",listener);
        newButton("9",listener);
        newButton("x",op);
        
        
        newButton("0",listener);
        newButton("/",op);
        newButton("=",op);
        newButton("Clean",op);
        
        add(panelNumeros);
        operacion = "=";
        
        
    }

    class addNumeros implements ActionListener {
        //Marcamos quién va a ser el objeto que reciba la acción, el que cambie, en este caso será la pantalla
        //Que definimos con Layout North a modo de visor de los numeros

            @Override
            public void actionPerformed(ActionEvent ae) {
                //Extraemos lo que se presiona de los botones
                String num = ae.getActionCommand();
                if (start) {
                    pant.setText("");
                    start = false;
                }
                pant.setText(pant.getText() + num);
            }
        }

    class Operacion implements ActionListener {

            //Acá veremos como influyen los botones que hagan alguna
            @Override
            public void actionPerformed(ActionEvent ae) {
                String action = ae.getActionCommand();
                calculo(Double.parseDouble(pant.getText()));
                operacion = action;
                start = true;
            }

            public void calculo(Double n) {
                if (operacion.equals("+")) {
                    total += n;
                } else if (operacion.equals("-")) {
                    total -= n;
                } else if (operacion.equals("x")) {
                    total *= n;
                } else if (operacion.equals("/")) {
                    total /= n;
                } else if (operacion.equals("=")){
                    total = n;
                } else if (operacion.equals("Clean")) {
                    total = 0;
                }
                pant.setText("" + total);
            }
        }

    class limpiarPantalla implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent ae) {
                pant.setText("0");
            }

        }
    
    public void newButton(String nombre, ActionListener listener) {
            JButton btn = new JButton(nombre);
            btn.addActionListener(listener);
            panelNumeros.add(btn);
    }
}
