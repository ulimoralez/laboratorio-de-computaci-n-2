package TrabajoIndividualBanco;

import java.io.Serializable;


/**
 *
 * @author Ulises Moralez - ulimoralez
 */
public class CuentaCorriente implements Serializable{
    private String titular;
    private double saldo;
    private int nrocuenta;
    
    public CuentaCorriente(String titular, double saldo, int nrocuenta){
        this.titular = titular;
        this.saldo = saldo;
        this.nrocuenta = nrocuenta;
    }
    public CuentaCorriente(){
        
    }
    //Atr Titular, solo obtenerlo ya que no podremos cambiarlo
    public String getTitular() {
        return titular;
    }
    //Atr Saldo, get saldo para obtenerlo y para ingresar el set
    public double getSaldo() {
        return saldo;
    }
    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
    public void retiro(double monto, CuentaCorriente cuenta){
        cuenta.setSaldo(saldo - monto);    
    }
    //Atr nrocuenta no tendrá getter ni setter, por consigna
    //ToString
    @Override
    public String toString() {
        return "Cuenta Corriente " + "titular= " + titular + ", saldo= " + saldo + ", nrocuenta= " + nrocuenta;
    }
    public boolean Transferencia(CuentaCorriente emisor, CuentaCorriente receptor, double monto){
        if (emisor.getSaldo() < monto){
            return false;
        }else{
            emisor.retiro(monto, receptor);
            receptor.setSaldo(saldo + monto);
            return true;
        }
    }
}
